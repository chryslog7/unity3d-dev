﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class main_cam : MonoBehaviour {
    public Camera mainCam, menuCam, mapCam;
    public Canvas menuCanvas;
    public GameObject arButtons, scheduler;
    public AudioSource menuAudio;
    public Light Sun, Moon;
    IEnumerator StartDay;


    public void Start()
    {
        skybox_day();
        StartDay = DayLight();            
    }


    // Update is called once per frame
    public void Update ()
    {
        StartCoroutine(StartDay);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (menuCam.enabled)
            {
                mainCam.enabled = true;
                arButtons.SetActive(true);
                EnableMenu(false);

            }
            else if (mapCam.enabled)
            {
                mainCam.enabled = true;
                arButtons.SetActive(true);
                scheduler.SetActive(false);
                mapCam.enabled = false;
                EnableMenu(false);
            }
            else
            {
                mainCam.enabled = false;
                arButtons.SetActive(false);
                //EnableHouseCam(false);
                //EnableKitchenCam(false);
                EnableMenu(true);

            }
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (mainCam.enabled)
                arButtons.SetActive(!arButtons.activeInHierarchy);         

        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (mainCam.enabled)
            {
                mapCam.enabled = true;
                mainCam.enabled = false;
                arButtons.SetActive(false);
            }
            else if(mapCam.enabled)
            {
                mapCam.enabled = false;
                scheduler.SetActive(false);
                mainCam.enabled = true;
                arButtons.SetActive(true);

            }

        }
    }

    public void skybox_day()
    {
        Material day_mat = Resources.Load("day", typeof(Material)) as Material;
        RenderSettings.skybox = day_mat;
        Invoke("skybox_afternoon", 11);
    }
    public void skybox_afternoon()
    {
        Material day_mat = Resources.Load("afternoon", typeof(Material)) as Material;
        RenderSettings.skybox = day_mat;
        Invoke("skybox_night", 11);
    }
    public void skybox_night()
    {
        Material night_mat = Resources.Load("night", typeof(Material)) as Material;
        RenderSettings.skybox = night_mat;
        Invoke("skybox_day", 8);
    }
    
    public void EnableMenu(bool state)
    {
        menuCam.enabled = state;
        menuCam.GetComponent<AudioListener>().enabled = state;
        menuCanvas.enabled = state;
        menuAudio.enabled = state;
    }

    public void EnableHouseCam(bool state)
    {
        /*coffee.gameObject.SetActive(state);
        Kitchen.gameObject.SetActive(state);
        LivingRoom.gameObject.SetActive(state);
        BedRoom1.gameObject.SetActive(state);
        BedRoom2.gameObject.SetActive(state);*/
    }

    IEnumerator DayLight()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Day at : " + Time.time);
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(1);
        Sun.gameObject.transform.RotateAround(Vector3.zero, Vector3.right, 12f * Time.deltaTime);
    }



}
