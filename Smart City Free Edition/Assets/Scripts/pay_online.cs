﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class pay_online : MonoBehaviour, IPointerDownHandler {
    public InputField num16dig,num3dig;
    public Dropdown month, year;

    public Text PayError;
    public Image completeForm;

	public void OnPointerDown(PointerEventData eventdata)
    {
        if (num16dig.text == ""){
                PayError.text = "Λάθος 16-ψήφιος αριθμός κάρτας.";
        } else if (num3dig.text == ""){
                PayError.text = "Λάθος 3-ψήφιος αριθμός κάρτας.";
        } else if (month.captionText.text == ""){
                PayError.text = "Λάθος αριθμός μήνα λήξης της κάρτας.";
        } else if(year.captionText.text == ""){
                PayError.text = "Λάθος αριθμός έτους λήξης κάρτας";
        }else{
                PayError.text = "Επιτυχής πληρωμή.";
                Invoke("endOrder",2);                
        }
        
    }
    public void endOrder()
    {
        completeForm.gameObject.SetActive(false);
    }
}
