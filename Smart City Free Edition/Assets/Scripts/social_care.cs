﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class social_care : MonoBehaviour,IPointerDownHandler {

    public Button message, call;
    public void OnPointerDown(PointerEventData eventData)
    {

        hideAlert();
        Invoke("showAlert", 10);
    }

    public void Start()
    {
        Invoke("showAlert",10);
    }

    public void showAlert()
    {
        message.gameObject.SetActive(true);
        call.gameObject.SetActive(false);
    }
    public void hideAlert()
    {
        message.gameObject.SetActive(false);
        call.gameObject.SetActive(true);
    }

}
