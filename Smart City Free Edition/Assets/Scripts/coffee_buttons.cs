﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class coffee_buttons : MonoBehaviour, IPointerDownHandler {

    public GameObject cap;
    public Camera camera1;
    public Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12;
    public Button water_back;
    public Button espresso,cappuccino,frappe,chocolate;
    public Button Served,Chosen;

    public void initButtons(){
        button1.gameObject.SetActive(true);
        button2.gameObject.SetActive(true);
        button4.gameObject.SetActive(true);
        button8.gameObject.SetActive(true);
        button3.gameObject.SetActive(false);
        button5.gameObject.SetActive(false);
        button6.gameObject.SetActive(false);
        button7.gameObject.SetActive(false);
        button9.gameObject.SetActive(false);
        button10.gameObject.SetActive(false);
        button11.gameObject.SetActive(false);
        button12.gameObject.SetActive(false);
        espresso.gameObject.SetActive(false);
        cappuccino.gameObject.SetActive(false);
        frappe.gameObject.SetActive(false);
        chocolate.gameObject.SetActive(false);
        Chosen.gameObject.SetActive(false);        
    }
        
    public void OnPointerDown(PointerEventData eventData)
    {      

        subButtons();
        if (gameObject.name == button1.name)
        {
            espresso.gameObject.SetActive(true);
            cappuccino.gameObject.SetActive(true);
            frappe.gameObject.SetActive(true);
            chocolate.gameObject.SetActive(true);
        }
        if (gameObject.name == button2.name)
        {
            water_back.gameObject.SetActive(true);
            openCap();
            button9.gameObject.SetActive(false);
        }        
        if (gameObject.name == button4.name)
        {
            button5.gameObject.SetActive(true);
            button6.gameObject.SetActive(true);
            button7.gameObject.SetActive(true);
        }

        if (gameObject.name == button5.name)
        {
            button5.gameObject.SetActive(false);
            button6.gameObject.SetActive(false);
            button7.gameObject.SetActive(false);
            button10.gameObject.SetActive(true);
            button11.gameObject.SetActive(true);
            button12.gameObject.SetActive(true);
        }

        if (gameObject.name == espresso.name || gameObject.name == cappuccino.name || gameObject.name == frappe.name || gameObject.name == chocolate.name)
        {
            Chosen.gameObject.SetActive(true);
        }

        if (gameObject.name == button6.name || gameObject.name == button7.name || gameObject.name == button10.name || gameObject.name == button11.name || gameObject.name == button12.name)
        {
            Served.gameObject.SetActive(true);
        }
        
        if (gameObject.name == button9.name && button9.isActiveAndEnabled)
        {
            initButtons();
            Served.gameObject.SetActive(false);
        }

        

    }

    void subButtons()
    {
        button1.gameObject.SetActive(false);
        button2.gameObject.SetActive(false);
        button4.gameObject.SetActive(false);
        button8.gameObject.SetActive(false);
        button9.gameObject.SetActive(true);
    }

    void openCap()
    {
        cap.transform.Rotate(-90, 0, 0);
        camera1.transform.position += new Vector3(0, (float)0.444, 0);
        camera1.transform.Rotate(15, 0, 0);
    }

    public void closeCap()
    {
        cap.transform.Rotate(90, 0, 0);
        camera1.transform.position += new Vector3(0, (float)-0.444, 0);
        camera1.transform.Rotate(-15, 0, 0);
    }

}
