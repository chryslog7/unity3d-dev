﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class water_back : MonoBehaviour, IPointerDownHandler
{
    public Button Served; 
    public coffee_buttons water;
    public void OnPointerDown(PointerEventData eventData)
    {
        water.closeCap();
        gameObject.SetActive(false);
        water.initButtons();
        water.button9.gameObject.SetActive(false);
        Served.gameObject.SetActive(false);
    }
}
