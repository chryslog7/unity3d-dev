﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class choose_product : MonoBehaviour, IPointerDownHandler
{
    public Image ProductList, OrderForm;
    public Text price1, price2, price3, price4, price5, price6, price7, price8, price9;
    public Text quant1, quant2, quant3, quant4, quant5, quant6, quant7, quant8, quant9;
    public Text quant_val, total_val;
    public bool sum;
    private double balance;
    private int amount;
    public void OnPointerDown(PointerEventData eventData)
    {
        if (sum) {
            Debug.Log("Not sum");
        } 
        else
        {
            //Zero quantity bug fixed - not charging default prices//
            if (double.Parse(quant1.text) == 0) price1.text = "0";
            if (double.Parse(quant2.text) == 0) price2.text = "0";
            if (double.Parse(quant3.text) == 0) price3.text = "0";
            if (double.Parse(quant4.text) == 0) price4.text = "0";
            if (double.Parse(quant5.text) == 0) price5.text = "0";
            if (double.Parse(quant6.text) == 0) price6.text = "0";
            if (double.Parse(quant7.text) == 0) price7.text = "0";
            if (double.Parse(quant8.text) == 0) price8.text = "0";
            if (double.Parse(quant9.text) == 0) price9.text = "0";
            amount = int.Parse(quant1.text) + int.Parse(quant2.text) + int.Parse(quant3.text) + int.Parse(quant4.text) + int.Parse(quant5.text) + int.Parse(quant6.text) + int.Parse(quant7.text) + int.Parse(quant8.text) + int.Parse(quant9.text);
            balance = double.Parse(price1.text) + double.Parse(price2.text) + double.Parse(price3.text) + double.Parse(price4.text) + double.Parse(price5.text) + double.Parse(price6.text) + double.Parse(price7.text) + double.Parse(price8.text) + double.Parse(price9.text);
            quant_val.text = amount.ToString();
            total_val.text = balance.ToString();

            
        }
        ProductList.gameObject.SetActive(!ProductList.isActiveAndEnabled);
        OrderForm.gameObject.SetActive(!OrderForm.isActiveAndEnabled);
        sum = true;
    }

}
