﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class online_help_button : MonoBehaviour, IPointerDownHandler
{
    public Image HelpForm;

    public void OnPointerDown(PointerEventData eventData)
    {
        HelpForm.gameObject.SetActive(true);
    }

}
