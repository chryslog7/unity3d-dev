﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class save_scheduler : MonoBehaviour, IPointerDownHandler {

    //public InputField Comments;
    //public Dropdown choose_hour,choose_time,choose_locat,choose_veh;
    public GameObject schedulerForm;
    public Button planSuccess;
    // Use this for initialization
    public void OnPointerDown(PointerEventData eventData)
    {
        Invoke("endScheduler", 1); 
    }

    void endScheduler()
    {
        schedulerForm.SetActive(false);
    }

}
