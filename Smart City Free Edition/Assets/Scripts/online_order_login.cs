﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class online_order_login : MonoBehaviour, IPointerDownHandler
{

    public Image LoginForm, ProductList;
    public InputField email, pass;
    public Text errors;
    public void OnPointerDown(PointerEventData eventData)
    {        
        if (email.text == "") errors.text = "** Συμπλήρωσε τη διεύθυνση email σωστά. **";
        else if (pass.text == "") errors.text = "** Συμπλήρωσε τον κωδικό πρόσβασης σωστά. **";
        else
        {
            LoginForm.gameObject.SetActive(!LoginForm.isActiveAndEnabled);
            ProductList.gameObject.SetActive(!ProductList.isActiveAndEnabled);
        }
    }

}