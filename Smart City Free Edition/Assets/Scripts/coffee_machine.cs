﻿using UnityEngine;
using UnityEngine.EventSystems;// Required when using Event data.
using UnityEngine.UI;

public class coffee_machine : MonoBehaviour {

    public Camera mainCam,mapView,menuCam,coffeeView, bath1, bath2, br1, br2;
    public Canvas coffeeMenu;
    
    void OnMouseDown()
    {
        mainCam.enabled = false;
        mapView.enabled = false;
        menuCam.enabled = false;
        bath1.enabled = false;
        bath2.enabled = false;
        br1.enabled = false;
        br2.enabled = false;
        coffeeView.enabled = true;
        coffeeMenu.gameObject.SetActive(true);
    }

}
