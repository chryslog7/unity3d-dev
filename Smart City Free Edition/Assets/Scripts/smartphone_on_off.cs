﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class smartphone_on_off : MonoBehaviour,IPointerDownHandler {
    public Image Smartphone;
    public Text walletPrice;

    public void OnPointerDown(PointerEventData eventData)
    {
        
        Smartphone.gameObject.SetActive(!Smartphone.isActiveAndEnabled);
        if (!Smartphone.isActiveAndEnabled) walletPrice.enabled = false;
    }
       
}
