﻿using UnityEngine;
using UnityEngine.UI;

public class on_off_coffee_machine : MonoBehaviour
{
    [SerializeField]
    private bool off = true;
    public Canvas canvas1;

    void OnMouseDown()
    {
        if (off)
        {
            canvas1.gameObject.SetActive(true);
            off = false;
            gameObject.transform.Rotate(0,0,(float)3.6);
        }
        else
        {
            canvas1.gameObject.SetActive(false);
            off = true;
            gameObject.transform.Rotate(0, 0, (float)-3.6);
        }
    }
}