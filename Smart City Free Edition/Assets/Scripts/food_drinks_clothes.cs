﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class food_drinks_clothes : MonoBehaviour {

    public Dropdown ProductListItems;
    public Image Foods, Drinks, Clothes;
    public Button SubmitOrder;
	// Use this for initialization
	void Start () {
        ProductListItems.onValueChanged.AddListener(delegate
        {
            if (ProductListItems.value == 0) {
                Foods.gameObject.SetActive(true); 
                Drinks.gameObject.SetActive(false);
                Clothes.gameObject.SetActive(false);
            }
            if (ProductListItems.value == 1) {
                Foods.gameObject.SetActive(false);
                Drinks.gameObject.SetActive(true);
                Clothes.gameObject.SetActive(false);
            }
            if (ProductListItems.value == 2)
            {
                Foods.gameObject.SetActive(false);
                Drinks.gameObject.SetActive(false);
                Clothes.gameObject.SetActive(true);
            }
        });
	}
}
