﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class house_button : MonoBehaviour, IPointerDownHandler
{
    public Camera MainCamera, Kitchen, LivingRoom, BedRoom1, BedRoom2;
    public Canvas coffee;
    private float margin = (float)0.5;

    /*public void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            coffee.enabled = false;
            Kitchen.enabled = false;
            LivingRoom.enabled = false;
            BedRoom1.enabled = false;
            BedRoom2.enabled = false;
            MainCamera.enabled = true;

        }

    }*/

    public void OnPointerDown(PointerEventData eventData)
    {
        
        MainCamera.enabled = false;
        Kitchen.enabled = true;
        LivingRoom.enabled = true;
        BedRoom1.enabled = true;
        BedRoom2.enabled = true;
        Kitchen.rect = new Rect(0,0,margin,margin);
        LivingRoom.rect = new Rect(margin, 0, margin, margin);
        BedRoom1.rect = new Rect(0,margin, margin, margin);
        BedRoom2.rect = new Rect(margin, margin, margin, margin);        
    }

    

}
