﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class online_order_back : MonoBehaviour, IPointerDownHandler {

    public Image OnlineOrder;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnlineOrder.gameObject.SetActive(false);
    }
	
}
