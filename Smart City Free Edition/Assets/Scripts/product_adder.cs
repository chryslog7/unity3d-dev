﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class product_adder : MonoBehaviour, IPointerDownHandler
{
    public Text price, quantity;
    private double price2, quantity2;
    //public double quant2Total, price2Total;

    public Button sumTotal;
    public Text quantityVal, priceVal;

    public void OnPointerDown(PointerEventData eventData)
    {
        sumTotal.gameObject.SetActive(true);
        price2 = double.Parse(price.text);
        quantity2 = double.Parse(quantity.text);        
        if (quantity2 != 0) {
            price2 = price2 / quantity2;
        }
        quantity2++;
        price.text = (price2 * quantity2).ToString();
        quantity.text = quantity2.ToString();
        //quant2Total += quantity2;
        //price2Total += price2; 
               
    }
}
