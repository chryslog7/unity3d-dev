﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class back_btn : MonoBehaviour, IPointerDownHandler {
    public Camera menuCam, mainCam;

    public void OnPointerDown(PointerEventData eventData)
    {
        mainCam.enabled = true;
        mainCam.gameObject.GetComponent<main_cam>().EnableMenu(false);
    }
        
}
