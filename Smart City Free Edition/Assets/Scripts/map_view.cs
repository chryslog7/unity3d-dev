﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class map_view : MonoBehaviour,IPointerDownHandler {

    public Light Sun, Moon;
    public Camera Main_camera, MapCamera;
    public GameObject src_loc, dest_loc, arButtons;    
    public List<GameObject> road;
    public List<float> posX,posZ,dist1,dist2;
    public List<string> roadName;
    //public Text now_stamp;

    public void Start () {
        home2univ();
        // now_stamp.text = DateTime.Now.ToString("yyyyMMddHHmmss");
        Debug.Log("map");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Main_camera.enabled = false;
        MapCamera.enabled = true;
        arButtons.SetActive(false);
        
    }


    public void home2univ() {
        road.Add(GameObject.Find("9"));
        road.Add(GameObject.Find("14"));
        road.Add(GameObject.Find("24"));
        road.Add(GameObject.Find("65"));
        road.Add(GameObject.Find("66"));
        road.Add(GameObject.Find("67"));

        foreach (GameObject i in road)
        {
            i.GetComponent<Renderer>().material = null;
            i.GetComponent<Renderer>().material.color = new Color32(60, 242, 24, 1);
            posX.Add(i.transform.position.x);
            posZ.Add(i.transform.position.z);
            roadName.Add(i.name);
        }

        float[] positX = posX.ToArray();
        float[] positZ = posZ.ToArray();
        for(int j = 0;j<positX.Length;j++)
        {
            dist1.Add(distance(src_loc.transform.position.x, src_loc.transform.position.z, positX[j], positZ[j]));
            dist2.Add(distance(dest_loc.transform.position.x, dest_loc.transform.position.z, positX[j], positZ[j]));
            //Debug.Log(distance(user_loc.transform.position.x, user_loc.transform.position.z, positX[0], positZ[0]) + " , " + roadName[0]);
            //Debug.Log(distance(user_loc.tranform.position.x, user_loc.transform.position.z, positX[1], positZ[1]) + " , " + roadName[1]);
            //Debug.Log(distance(user_loc.transform.position.x, user_loc.transform.position.z, positX[2], positZ[2]) + " , " + roadName[2]);
            //Debug.Log(distance(user_loc.transform.position.x, user_loc.transform.position.z, positX[3], positZ[3]) + " , " + roadName[3]);
            //Debug.Log(distance(user_loc.transform.position.x, user_loc.transform.position.z, positX[4], positZ[4]) + " , " + roadName[4]);
            //Debug.Log(distance(user_loc.transform.position.x, user_loc.transform.position.z, positX[5], positZ[5]) + " , " + roadName[5]);
        }
        //string[] roadN1 = roadName.ToArray();
        //string[] roadn2 = roadName.ToArray();

        //float[] dists = dist1.ToArray();
        //float[] dists2 = dist2.ToArray();

        //float temp, temp2;
        //string roadN, road_n2;
        
        //float[] distSort = new float[dists.Length];
        //for (int j = 0; j < dists.Length; j++)
        //{
        //    for (int k = 0; k < dists.Length - 1; k++)
        //    {
        //        if (dists[k] > dists[k + 1])
        //        {
        //            temp = dists[k + 1];
        //            dists[k + 1] = dists[k];
        //            dists[k] = temp;
        //            roadN = roadN1[k+1];
        //            roadN1[k + 1] = roadN1[k];
        //            roadN1[k] = roadN;
        //        }
        //    }
        //}

        //for (int j = 0; j < dists2.Length; j++)
        //{
        //    //Debug.Log(dists2[j] + " , " + roadn2[j]);
        //}

        //float[] distSort2 = new float[dists2.Length];
        //for (int j = 0; j < dists2.Length; j++)
        //{
        //    for (int k = 0; k < dists.Length - 1; k++)
        //    {
        //        if (dists2[k] > dists2[k + 1])
        //        {
        //            temp2 = dists2[k + 1];
        //            dists2[k + 1] = dists2[k];
        //            dists2[k] = temp2;
        //            road_n2 = roadn2[k + 1];
        //            roadn2[k + 1] = roadn2[k];
        //            roadn2[k] = road_n2;
        //        }
        //    }
        //}

        //for (int j = 0; j < dists2.Length; j++)
        //{
           // Debug.Log(dists2[j] + " , " + roadn2[j]);
        //}

    }

    
    float distance(float x1,float z1,float x2,float z2)
    {
        float result = Mathf.Sqrt(Mathf.Pow(x2 - x1,2) + Mathf.Pow(z1 - z2,2));
        return result;
    }
    

}
