﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class wallet_balance : MonoBehaviour, IPointerDownHandler
{
    public Text totalPrice, walletPrice;
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!totalPrice.text.Equals("0"))
        {
            double balance = double.Parse(walletPrice.text);
            double total = double.Parse(totalPrice.text);
            if(!walletPrice.text.Equals(null)) walletPrice.text = (balance - total).ToString();
        }
        else
        {
            walletPrice.enabled = !walletPrice.enabled;
            walletPrice.text = "1002€";
        }
    }
}
