﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class help_back_button : MonoBehaviour, IPointerDownHandler {

    public Image HelpForm;

    public void OnPointerDown(PointerEventData eventData)
    {
        HelpForm.gameObject.SetActive(false);
    }
}
