﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class accountInfo : MonoBehaviour, IPointerDownHandler
{
    public Image ProductList, OrderForm, PurchaseForm;
    public InputField firstname, lastname, address, number;
    public Text FieldErrors;

    public void OnPointerDown(PointerEventData eventData)
    {
        
        if (firstname.text == ""){
            FieldErrors.text = "Συμπλήρωσε σωστά το πεδίο Όνομα.";
        }else if(lastname.text == ""){
            FieldErrors.text = "Συμπλήρωσε σωστά το πεδίο Επώνυμο.";
        }
        else if (address.text == ""){
            FieldErrors.text = "Συμπλήρωσε σωστά το πεδίο Email.";
        }
        else if(number.text == ""){
            FieldErrors.text = "Συμπλήρωσε σωστά το πεδίο Αριθμός.";
        }else{
            ProductList.gameObject.SetActive(false);
            OrderForm.gameObject.SetActive(false);
            PurchaseForm.gameObject.SetActive(true);
        }
    }

}
