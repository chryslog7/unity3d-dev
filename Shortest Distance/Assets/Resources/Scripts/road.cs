﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class road : MonoBehaviour
{
    public List<string> col_objs = new List<string>();
    public List<string> basic_col;
    public List<string> buildings = new List<string>(){"Home","University"};

    void OnCollisionEnter(Collision col)
    {
        basic_col = new List<string>(){gameObject.name,"Ground","Player"};
        if(!basic_col.Contains(col.gameObject.name))
        {
            col_objs.Add(col.gameObject.name);
        }
    }
    
}
