﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class gps : MonoBehaviour
{
    List<string> basic_col;
    public List<string> col_objs,buildings;
    public int col_count;
    public bool col_stat, MapOn;
    public string dest;
    GameObject roads;

    void OnCollisionEnter(Collision col)
    {
        col_stat = true;
        if(!basic_col.Contains(col.gameObject.name))
        {
            col_objs.Add(col.gameObject.name);
            col_count++;
            if(!buildings.Contains(col.gameObject.name))
            {
                OnRoad(col.gameObject.name);
            }
        }

    }

    void OnCollisionExit(Collision col)
    {
        RemoveCollider(col.gameObject.name,col_objs);
        col_stat = false;
        col_count--;
        if(!buildings.Contains(col.gameObject.name)) OffRoad(col.gameObject.name);
        ResetValues();
    }

    void ResetValues()
    {
        basic_col = new List<string>(){gameObject.name,"Terrain"};
        buildings = new List<string>(){"Home","Apartments"};
        col_objs = new List<string>();
        col_count = 0;
        col_objs.Clear();
                  //MapOn = false;
        roads = GameObject.Find("Roads");
        foreach(Transform street in roads.transform)
        {
          OffRoad(street.name);
        }
        ShowLine(gameObject.transform.position,gameObject.transform.position);
    }

    void RemoveCollider(string col_name,List<string> col_objs)
    {
        for(int i=0;i<col_objs.Count;i++)
        {
            if(col_objs[i].Equals(col_name))
            {
              col_objs.RemoveAt(i);
            }
        }
    }

    void RoadPoint(GameObject object1, GameObject object2)
    {
        Collider col1 = object1.GetComponent<Collider>();
        Collider col2 = object2.GetComponent<Collider>();
        Vector3 pos1 = object1.transform.position;
        Vector3 pos2 = object2.transform.position;
        Vector3 p1 = col1.ClosestPointOnBounds(pos2);
        Vector3 p2 = col2.ClosestPointOnBounds(pos1);
        //float distance = Vector3.Distance(p1, p2);
        //Debug.Log(distance + " ,"+object1.name+" | "+object2.name);
        ShowLine(p1,p2);
    }

    void ShowLine(Vector3 start, Vector3 end, float startWidth = 0.2f, float endWidth = 0.2f)
    {
       LineRenderer startLine;
       if (gameObject.GetComponent<LineRenderer>() != null)
           startLine = gameObject.GetComponent<LineRenderer>();
       else
           startLine = gameObject.AddComponent<LineRenderer>();
       startLine.startWidth = startWidth;
       startLine.endWidth = endWidth;
       startLine.SetPosition(0, start);
       startLine.SetPosition(1, end);
    }

    void OnRoad(string street)
    {
        GameObject streetObj = GameObject.Find(street);
        Renderer rend = streetObj.GetComponent<Renderer>();
        rend.material = Resources.Load("Materials/Blue_Light_2", typeof(Material)) as Material;
    }

    void OffRoad(string street)
    {
        GameObject streetObj = GameObject.Find(street);
        Renderer rend = streetObj.GetComponent<Renderer>();
        //rend.material = Resources.Load("Materials/Black", typeof(Material)) as Material;
    }

    void Start()
    {
        ResetValues();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
              ResetValues();
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            if(!MapOn)
            {
                Debug.Log("Map enabled");
                MapOn = true;
                GameObject.Find("MainCamera").GetComponent<Camera>().enabled = false;
                GameObject.Find("MapView").GetComponent<Camera>().enabled = true;
                Cursor.visible = true;
                GameObject.Find("Home").GetComponent<Collider>().enabled = true;
            }
            else
            {
                Debug.Log("Map disabled");
                MapOn = false;
                GameObject.Find("MainCamera").GetComponent<Camera>().enabled = true;
                GameObject.Find("MapView").GetComponent<Camera>().enabled = false;
                Cursor.visible = false;
                GameObject.Find("Home").GetComponent<Collider>().enabled = false;
                ResetValues();
            }
        }
        if(Input.GetMouseButtonDown(0))
        {
            if(MapOn)
            {
                Ray ray = GameObject.Find("MapView").GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if(buildings.Contains(hit.collider.gameObject.name))
                    {
                        ResetValues(); //clear previous route
                        dest = hit.collider.gameObject.name;
                        Debug.Log("Destination selected. " + dest);
                        FindRoute();
                    }
                }
            }
        }
        // if(Input.GetMouseButtonDown(1))
        // {
        //     ResetValues();
        //     MapOn = false;
        // }
        // if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        // {
        //     Vector3 position = this.transform.position;
        //     position.z -= 0.5f;
        //     transform.position = position;
        // }
        // if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        // {
        //   Vector3 position = transform.position;
        //   position.z += 0.5f;
        //   transform.position = position;
        // }
        // if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        // {
        //    Vector3 position = transform.position;
        //    position.x += 0.5f;
        //    transform.position = position;
        // }
        // if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        // {
        //    Vector3 position = transform.position;
        //    position.x -= 0.5f;
        //    transform.position = position;
        // }


    }

          //Pathfindind begins
    float ClosestPoint(GameObject src, GameObject dst)
    {
        Collider col1 = src.GetComponent<Collider>();
        Vector3 pos1 = src.transform.position;
        Collider col2 = dst.GetComponent<Collider>();
        Vector3 pos2 = dst.transform.position;
        Vector3 p1 = col1.ClosestPointOnBounds(pos2);
        Vector3 p2 = col2.ClosestPointOnBounds(pos1);
        float distance = Vector3.Distance(p1, p2);
        return distance;
    }

    string ClosestRoad(GameObject src)
    {
        string road_name = roads.transform.GetChild(0).name;
        float distance = ClosestPoint(src,roads.transform.GetChild(0).gameObject);

        float min_dist = distance;
        foreach(Transform street in roads.transform)
        {
            if(!street.name.Contains(src.gameObject.name))
            {

                distance = ClosestPoint(src,street.gameObject);
                if(distance<min_dist)
                {
                    min_dist = distance;
                    road_name = street.name;
                }

              }
          }
          OnRoad(road_name);
          return road_name;
    }

    string[] Road2Road(GameObject src,GameObject dst)
    {
        if(dst==null)
        {
            return src.GetComponent<road>().col_objs.ToArray();
        }
                  //find closest road based on destination
        List<string> final_roads = new List<string>();
        List<string> col_roads = src.GetComponent<road>().col_objs;
        string road_name = col_roads[0];
        float distance = ClosestPoint(GameObject.Find(col_roads[0]),dst);
        float min_dist = distance;
        final_roads.Add(col_roads[0]);

        foreach(string street in col_roads)
        {
                          //return destination name to stop FindRoute method successfully
            if(street.Contains(dest))
            {
                                    //Debug.Log("Last Road");
                final_roads.Clear();
                final_roads.Add(street);
                return final_roads.ToArray();
            }
            else
            {
                                //lookup for minimum  distance from destination
                distance = ClosestPoint(GameObject.Find(street),dst);
                if(distance<min_dist)
                {
                    min_dist = distance;
                    road_name = street;
                    final_roads.Clear();
                    final_roads.Add(street);
                }

            }

        }
        return final_roads.ToArray();
    }

    public int offset = 1;
    void FindRoute()
    {
        string first_road = ClosestRoad(gameObject);
        RoadPoint(gameObject, GameObject.Find(first_road));
        //lookup roads until finding destination
        string last_road = first_road;
        //Debug.Log(GameObject.Find(first_road).name+""+GameObject.Find(dest).name);
        string[] next_road = Road2Road(GameObject.Find(first_road),GameObject.Find(dest));
        List<string> route = new List<string>();
        OnRoad(last_road);
        while(!next_road.Contains(dest) && offset < 10)
        {
            offset++;
            route.Add(last_road);
            next_road = Road2Road(GameObject.Find(last_road),GameObject.Find(dest));
            last_road = next_road[0]; //needs fix - to iterate col_roads until dest is found,then set last_road by index
            Debug.Log(last_road);

        }
        ShowRoute(route.ToArray());
    }

    void ShowRoute(string[] route)
    {
        foreach(string street in route)
        {
            OnRoad(street);
        }
    }


}
