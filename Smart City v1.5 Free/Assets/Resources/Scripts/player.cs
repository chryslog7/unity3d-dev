﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class player : MonoBehaviour
{
    public float timer;
    Animator animator;
    GameObject head;
    IEnumerator playerAnim;
    bool stopRunning = false;

    void Start()
    {
        playerAnim = StartAnimation("idle");
        head = GameObject.Find("Head");
        animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (!Input.anyKey)
        {
            playerAnim = StartAnimation("idle");
            StartCoroutine(playerAnim);
        }
        else if (Input.GetKey(KeyCode.C))
        {
            //force code if C is pressed
            if (Input.GetKeyDown(KeyCode.C))
            {
                //disable animator by kinematic to fix positioning bug
                GetComponent<Rigidbody>().isKinematic = true;
                animator.enabled = false;
            }
            if (Input.GetKeyUp(KeyCode.C))
            {
                //re-enable animator after C is released
                GetComponent<Rigidbody>().isKinematic = false;
                animator.enabled = true;
            }
        }
        else
        {
            //allow code only if C is released
            bool leftShiftKey = Input.GetKey(KeyCode.LeftShift);
            bool leftShiftKeyUp = Input.GetKeyUp(KeyCode.LeftShift);
            bool moveKeys = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow);

            if (moveKeys)
            {

                if (leftShiftKey)
                {
                    playerAnim = StartAnimation("run");
                    StartCoroutine(playerAnim);
                }
                else if (leftShiftKeyUp)
                {

                    if (!stopRunning)
                    {
                        playerAnim = StartAnimation("run2idle");
                        StartCoroutine(playerAnim);
                    }

                }
                else
                {

                    if (!stopRunning)
                    {
                        playerAnim = StartAnimation("walk");
                        StartCoroutine(playerAnim);
                    }

                }
            }
            else
            {

                if (!stopRunning)
                {
                    playerAnim = StartAnimation("idle");
                    StartCoroutine(playerAnim);
                }

            }


        }

    }

    IEnumerator StartAnimation(string anim_name)
    {
        animator.runtimeAnimatorController = Resources.Load("Controllers/"+anim_name) as RuntimeAnimatorController;
        float duration = animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
        animator.enabled = true;
        if (anim_name.Contains("run2idle")) //fix run2idle duration
        { 
            stopRunning = true; 
            duration-=1.5f; 
        } 
        yield return new WaitForSecondsRealtime(duration);
        if (anim_name.Contains("run2idle")) 
            stopRunning = false;
    }
}
