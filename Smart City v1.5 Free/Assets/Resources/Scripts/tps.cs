﻿using System.Collections;
using UnityEngine;

public class tps : MonoBehaviour
{
    public GameObject player;
    Rigidbody rb;
    float speed = 16f, camSpeed = 80f;
    string mouseX = "Mouse X", mouseY = "Mouse Y";
    GameObject head;
    Vector3 cameraPos;

    void Start()
    {
        Cursor.visible = false;
        cameraPos = transform.localPosition;
        rb = player.GetComponent<Rigidbody>();
        head = GameObject.Find("Head");
    }

    void FixedUpdate()
    {
        camMove = ChangeCamView();
        if (!Input.anyKey)
            StartCoroutine(camMove);
        else if (Input.GetKey(KeyCode.C))
        {
            Cursor.lockState = CursorLockMode.Locked;
            transform.localPosition = cameraPos;
            transform.LookAt(head.transform.position);
        }
        else
        {
            StopCoroutine(camMove);
            WASD();
            Arrows();
            MouseRotate();
        }
    }

    void WASD()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-transform.right * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(transform.right * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(-transform.forward * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.forward * speed);
            PlayerRotate();
        }
    }

    void Arrows()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddForce(-transform.right * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddForce(transform.right * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddForce(-transform.forward * speed);
            PlayerRotate();
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddForce(transform.forward * speed);
            PlayerRotate();
        }
    }

    void PlayerRotate()
    {
        float angle = Mathf.Atan2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Mathf.Rad2Deg;        
        transform.parent = null;
        player.transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y  + angle, 0);
        transform.SetParent(player.transform.GetChild(1));        
    }

    void MouseRotate()
    {
        if (Input.GetAxis(mouseX) != 0)
        {
            float angle2 = Mathf.Atan2(Input.GetAxis(mouseX), Input.GetAxis(mouseY));
            player.transform.localEulerAngles += new Vector3(0, angle2, 0);
        }
    }

    IEnumerator camMove;
    void CameraOnIdle(Vector3 point)
    {
        float angle = transform.eulerAngles.x;
        if (Input.GetAxis(mouseX) != 0)
        {
            transform.RotateAround(point, new Vector3(0.0f, 1.0f, 0.0f), Input.GetAxis(mouseX) * camSpeed * Time.deltaTime);
            transform.LookAt(point);
        }
        if (Input.GetAxis(mouseY) != 0)
        {
            transform.RotateAround(point, new Vector3(1.0f, 0.0f, 0.0f), -Input.GetAxis(mouseY) * camSpeed * Time.deltaTime);
            transform.LookAt(point);
        }
    }

    IEnumerator ChangeCamView()
    {
        CameraOnIdle(head.transform.position);
        yield return new WaitForSecondsRealtime(0.1f);
    }

}
